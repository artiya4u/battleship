const express = require('express');
const router = express.Router();

const Games = require('../models/Boards');
const Placement = require('../models/Placement');
const Attacks = require('../models/Attacks');

router.get('/new', async (req, res, next) => {
  let board = await Games.newGame();
  return res.send(board);
});

router.get('/status', async (req, res, next) => {
  let board = await Games.getBoardById(req.query.boardId);
  if (board === undefined) {
    res.status(404);
    return res.send({ 'status': 'no', 'message': 'Not found the game board' });
  } else {
    // get ships state.
    let playingBoard = Games.getEmptyBoard();
    let placementsFromDB = await Placement.getPlacements(req.query.boardId);
    Games.place(playingBoard, placementsFromDB);
    let pastAttacks = await Attacks.getAttacks(board.id);
    Games.attack(playingBoard, pastAttacks);
    let status = {
      boardId: board.id,
      status: Games.gameStatusText(board.gameStatus),
      move: board.move,
    };
    // Flip array to view as user.
    status.state = playingBoard.state[0].map((col, i) => playingBoard.state.map(row => row[i]));
    // Show fleet status only for defender.
    if (board.defenderId.toFixed(0) === req.query.defenderId) {
      status.fleet = playingBoard.fleet;
    }
    return res.send(status);
  }
});

function printMap (array) {
  let map = array[0].map((col, i) => array.map(row => row[i]));
  console.table(map);
}

router.post('/place', async (req, res, next) => {
  let placement = req.body;
  // Validate board id
  let board = await Games.getBoardById(placement.boardId);
  if (board === undefined) {
    res.status(404);
    return res.send({ 'status': 'no', 'message': 'Not found the game board' });
  } else {
    // Verify defenderId
    if (board.defenderId !== parseInt(placement.defenderId)) {
      res.status(403);
      return res.send({ 'status': 'no', 'message': 'Only valid defender can place the ships.' });
    } else {
      try {
        let placingBoard = Games.getEmptyBoard();
        // Get all past placement
        let placementsFromDB = await Placement.getPlacements(placement.boardId);
        let placements = [];
        for (let p of placementsFromDB) {
          placements.push({ ...p });
        }
        placements.push(placement);
        let finishPlacement = Games.place(placingBoard, placements);
        await Placement.insertPlacement(placement);
        if (finishPlacement) {
          board.gameStatus = Games.gameState.READY;
          await Games.updateBoard(board);
        }
        res.send({ 'status': 'ok', 'message': 'ships placed', 'finishPlacement': finishPlacement });
      } catch (e) {
        res.status(400);
        return res.send({ 'status': 'no', 'message': e.message });
      }
    }
  }
});

router.post('/attack', async (req, res, next) => {
  let attack = req.body;
  // Validate board id
  let board = await Games.getBoardById(attack.boardId);
  if (board === undefined || board.attackerId !== parseInt(attack.attackerId)) {
    res.status(403);
    return res.send({ 'status': 'no', 'message': 'Only valid attacker can attack this game.' });
  } else {
    if (board.gameStatus === Games.gameState.NEW) {
      res.status(400);
      return res.send({ 'status': 'no', 'message': 'Can attack after defender finish feet placement.' });
    } else if (board.gameStatus === Games.gameState.FINISH) {
      res.status(400);
      return res.send({ 'status': 'no', 'message': 'The game has ended.' });
    }
    try {
      let playingBoard = Games.getEmptyBoard();
      let placementsFromDB = await Placement.getPlacements(attack.boardId);
      Games.place(playingBoard, placementsFromDB);
      let pastAttacks = await Attacks.getAttacks(board.id);
      let attacks = [];
      for (let atk of pastAttacks) {
        attacks.push({ ...atk }); // Remove raw packet object.
      }
      attacks.push(attack);
      let result = Games.attack(playingBoard, attacks);
      await Attacks.insertAttack(attack);
      console.log('Map:');
      printMap(playingBoard.map);
      console.log('State:');
      printMap(playingBoard.state);
      if (result.won) {
        board.gameStatus = Games.gameState.FINISH;
      }
      board.move = attacks.length;
      await Games.updateBoard(board);
      return res.send(result);
    } catch (e) {
      res.status(400);
      return res.send({ 'status': 'no', 'message': e.message });
    }
  }
});

module.exports = router;
