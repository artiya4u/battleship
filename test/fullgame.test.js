const request = require('supertest');

const app = require('../app');
describe('Battleship game', function () {
  it('should able to play full game', async function (done) {
    const { body } = await request(app)
      .get('/game/new')
      .expect('Content-Type', /json/)
      .expect(200);

    const BATTLESHIP = {
      'boardId': body.boardId,
      'defenderId': body.defenderId,
      'shipType': 'BATTLESHIP',
      'arrange': 'vertical',
      'coordinate': 'A1',
    };

    await request(app)
      .post('/game/place')
      .set({
        'content-type': 'application/json',
      })
      .send(BATTLESHIP)
      .expect('Content-Type', /json/)
      .expect(200);

    const CRUISER = {
      'boardId': body.boardId,
      'defenderId': body.defenderId,
      'shipType': 'CRUISER',
      'arrange': 'vertical',
      'coordinate': 'A3',
    };

    await request(app)
      .post('/game/place')
      .set({
        'content-type': 'application/json',
      })
      .send(CRUISER)
      .expect('Content-Type', /json/)
      .expect(200);

    CRUISER.coordinate = 'A5';
    await request(app)
      .post('/game/place')
      .set({
        'content-type': 'application/json',
      })
      .send(CRUISER)
      .expect('Content-Type', /json/)
      .expect(200);

    const SUBMARINE = {
      'boardId': body.boardId,
      'defenderId': body.defenderId,
      'shipType': 'SUBMARINE',
      'arrange': 'horizontal',
      'coordinate': 'A8',
    };

    await request(app)
      .post('/game/place')
      .set({
        'content-type': 'application/json',
      })
      .send(SUBMARINE)
      .expect('Content-Type', /json/)
      .expect(200);

    SUBMARINE.coordinate = 'C8';
    await request(app)
      .post('/game/place')
      .set({
        'content-type': 'application/json',
      })
      .send(SUBMARINE)
      .expect('Content-Type', /json/)
      .expect(200);

    SUBMARINE.coordinate = 'E8';
    await request(app)
      .post('/game/place')
      .set({
        'content-type': 'application/json',
      })
      .send(SUBMARINE)
      .expect('Content-Type', /json/)
      .expect(200);

    SUBMARINE.coordinate = 'J8';
    await request(app)
      .post('/game/place')
      .set({
        'content-type': 'application/json',
      })
      .send(SUBMARINE)
      .expect('Content-Type', /json/)
      .expect(200);

    const DESTROYER = {
      'boardId': body.boardId,
      'defenderId': body.defenderId,
      'shipType': 'DESTROYER',
      'arrange': 'vertical',
      'coordinate': 'A7',
    };

    DESTROYER.coordinate = 'I1';
    await request(app)
      .post('/game/place')
      .set({
        'content-type': 'application/json',
      })
      .send(DESTROYER)
      .expect('Content-Type', /json/)
      .expect(200);

    DESTROYER.coordinate = 'I3';
    await request(app)
      .post('/game/place')
      .set({
        'content-type': 'application/json',
      })
      .send(DESTROYER)
      .expect('Content-Type', /json/)
      .expect(200);

    DESTROYER.coordinate = 'I5';
    let res = await request(app)
      .post('/game/place')
      .set({
        'content-type': 'application/json',
      })
      .send(DESTROYER)
      .expect('Content-Type', /json/)
      .expect(200);
    expect(res.body.status).toBe('ok');
    expect(res.body.message).toBe('ships placed');
    expect(res.body.finishPlacement).toBe(true);

    let statusRes = await request(app)
      .get(`/game/status?boardId=${body.boardId}`)
      .expect('Content-Type', /json/)
      .expect(200);

    expect(statusRes.body.boardId).toBe(body.boardId);
    expect(statusRes.body.status).toBe('READY TO ATTACK');
    expect(statusRes.body.move).toBe(0);

    // Loop attack all position
    let char = 'ABCDEFGHIJ';
    for (let i = 0; i < 10; i += 1) {
      for (let j = 0; j < 10; j += 1) {
        let coordinate = `${char.charAt(i)}${j + 1}`;
        const attack = {
          'boardId': body.boardId,
          'attackerId': body.attackerId,
          'coordinate': coordinate,
        };

        let attRes = await request(app)
          .post('/game/attack')
          .set({
            'content-type': 'application/json',
          })
          .send(attack)
          .expect('Content-Type', /json/)
          .expect(200);

        if (coordinate === 'J10') {
          expect(attRes.body.won).toBe(true);
          expect(attRes.body.result).toBe('Win! You have completed the game in 100 moves');
        }
      }
    }
    return await done();// eslint-disable-line
  });
});
