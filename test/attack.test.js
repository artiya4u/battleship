const request = require('supertest');

const app = require('../app');
let boardId = 0;
let attackerId = 0;

describe('Attacker', function () {
  it('should NOT able to attack unknown board', async function (done) {
    const attack = {
      'boardId': 8888,
      'attackerId': 8888,
      'coordinate': 'F4',
    };

    let res = await request(app)
      .post('/game/attack')
      .set({
        'content-type': 'application/json',
      })
      .send(attack)
      .expect('Content-Type', /json/)
      .expect(403);

    expect(res.body.status).toBe('no');
    expect(res.body.message).toBe('Only valid attacker can attack this game.');
    return await done();// eslint-disable-line
  });

  it('should NOT able to attack no defend fleet board', async function (done) {
    const { body } = await request(app)
      .get('/game/new')
      .expect('Content-Type', /json/)
      .expect(200);

    const attack = {
      'boardId': body.boardId,
      'attackerId': body.attackerId,
      'coordinate': 'F4',
    };

    let res = await request(app)
      .post('/game/attack')
      .set({
        'content-type': 'application/json',
      })
      .send(attack)
      .expect('Content-Type', /json/)
      .expect(400);

    expect(res.body.status).toBe('no');
    expect(res.body.message).toBe('Can attack after defender finish feet placement.');
    return await done();// eslint-disable-line
  });

  it('should NOT able to attack while defender placing ships.', async function (done) {
    const { body } = await request(app)
      .get('/game/new')
      .expect('Content-Type', /json/)
      .expect(200);

    const BATTLESHIP = {
      'boardId': body.boardId,
      'defenderId': body.defenderId,
      'shipType': 'BATTLESHIP',
      'arrange': 'vertical',
      'coordinate': 'A1',
    };

    await request(app)
      .post('/game/place')
      .set({
        'content-type': 'application/json',
      })
      .send(BATTLESHIP)
      .expect('Content-Type', /json/)
      .expect(200);

    const attack = {
      'boardId': body.boardId,
      'attackerId': body.attackerId,
      'coordinate': 'F4',
    };

    let res = await request(app)
      .post('/game/attack')
      .set({
        'content-type': 'application/json',
      })
      .send(attack)
      .expect('Content-Type', /json/)
      .expect(400);

    expect(res.body.status).toBe('no');
    expect(res.body.message).toBe('Can attack after defender finish feet placement.');
    return await done();// eslint-disable-line
  });

  it('should NOT able to attack finished game.', async function (done) {
    const attack = {
      'boardId': 2,
      'attackerId': 2,
      'coordinate': 'F4',
    };

    let res = await request(app)
      .post('/game/attack')
      .set({
        'content-type': 'application/json',
      })
      .send(attack)
      .expect('Content-Type', /json/)
      .expect(400);

    expect(res.body.status).toBe('no');
    expect(res.body.message).toBe('The game has ended.');
    return await done();// eslint-disable-line
  });
});

describe('Attacker', function () {
  it('should able to attack within aboard', async function (done) {
    const attack = {
      'boardId': boardId,
      'attackerId': attackerId,
      'coordinate': 'F4',
    };

    let res = await request(app)
      .post('/game/attack')
      .set({
        'content-type': 'application/json',
      })
      .send(attack)
      .expect('Content-Type', /json/)
      .expect(200);

    expect(res.body.won).toBe(false);
    expect(res.body.result).toBe('Miss');
    return await done();// eslint-disable-line
  });

  it('should able to attack and hit', async function (done) {
    const attack = {
      'boardId': boardId,
      'attackerId': attackerId,
      'coordinate': 'A1',
    };

    let res = await request(app)
      .post('/game/attack')
      .set({
        'content-type': 'application/json',
      })
      .send(attack)
      .expect('Content-Type', /json/)
      .expect(200);

    expect(res.body.won).toBe(false);
    expect(res.body.result).toBe('Hit');
    return await done();// eslint-disable-line
  });

  it('should able to attack, hit and sank a ship', async function (done) {
    const attack = {
      'boardId': boardId,
      'attackerId': attackerId,
      'coordinate': '',
    };

    attack.coordinate = 'A1';
    await request(app)
      .post('/game/attack')
      .set({
        'content-type': 'application/json',
      })
      .send(attack)
      .expect('Content-Type', /json/)
      .expect(200);

    attack.coordinate = 'B1';
    await request(app)
      .post('/game/attack')
      .set({
        'content-type': 'application/json',
      })
      .send(attack)
      .expect('Content-Type', /json/)
      .expect(200);

    attack.coordinate = 'C1';
    await request(app)
      .post('/game/attack')
      .set({
        'content-type': 'application/json',
      })
      .send(attack)
      .expect('Content-Type', /json/)
      .expect(200);

    attack.coordinate = 'D1';
    let res = await request(app)
      .post('/game/attack')
      .set({
        'content-type': 'application/json',
      })
      .send(attack)
      .expect('Content-Type', /json/)
      .expect(200);

    expect(res.body.won).toBe(false);
    expect(res.body.result).toBe('You just sank a BATTLESHIP');
    return await done();// eslint-disable-line
  });

  it('should NOT able to attack the same place that already attack', async function (done) {
    const attack = {
      'boardId': boardId,
      'attackerId': attackerId,
      'coordinate': 'F4',
    };

    let res = await request(app)
      .post('/game/attack')
      .set({
        'content-type': 'application/json',
      })
      .send(attack)
      .expect('Content-Type', /json/)
      .expect(200);

    expect(res.body.won).toBe(false);
    expect(res.body.result).toBe('Miss');

    let res2 = await request(app)
      .post('/game/attack')
      .set({
        'content-type': 'application/json',
      })
      .send(attack)
      .expect('Content-Type', /json/)
      .expect(400);

    expect(res2.body.status).toBe('no');
    expect(res2.body.message).toBe('Cannot attack at the same place that already attack: F4');
    return await done();// eslint-disable-line
  });

  beforeEach(async () => {
    const { body } = await request(app)
      .get('/game/new')
      .expect('Content-Type', /json/)
      .expect(200);

    boardId = body.boardId;
    attackerId = body.attackerId;

    const BATTLESHIP = {
      'boardId': body.boardId,
      'defenderId': body.defenderId,
      'shipType': 'BATTLESHIP',
      'arrange': 'vertical',
      'coordinate': 'A1',
    };

    await request(app)
      .post('/game/place')
      .set({
        'content-type': 'application/json',
      })
      .send(BATTLESHIP)
      .expect('Content-Type', /json/)
      .expect(200);

    const CRUISER = {
      'boardId': body.boardId,
      'defenderId': body.defenderId,
      'shipType': 'CRUISER',
      'arrange': 'vertical',
      'coordinate': 'A3',
    };

    await request(app)
      .post('/game/place')
      .set({
        'content-type': 'application/json',
      })
      .send(CRUISER)
      .expect('Content-Type', /json/)
      .expect(200);

    CRUISER.coordinate = 'A5';
    await request(app)
      .post('/game/place')
      .set({
        'content-type': 'application/json',
      })
      .send(CRUISER)
      .expect('Content-Type', /json/)
      .expect(200);

    const SUBMARINE = {
      'boardId': body.boardId,
      'defenderId': body.defenderId,
      'shipType': 'SUBMARINE',
      'arrange': 'horizontal',
      'coordinate': 'A8',
    };

    await request(app)
      .post('/game/place')
      .set({
        'content-type': 'application/json',
      })
      .send(SUBMARINE)
      .expect('Content-Type', /json/)
      .expect(200);

    SUBMARINE.coordinate = 'C8';
    await request(app)
      .post('/game/place')
      .set({
        'content-type': 'application/json',
      })
      .send(SUBMARINE)
      .expect('Content-Type', /json/)
      .expect(200);

    SUBMARINE.coordinate = 'E8';
    await request(app)
      .post('/game/place')
      .set({
        'content-type': 'application/json',
      })
      .send(SUBMARINE)
      .expect('Content-Type', /json/)
      .expect(200);

    SUBMARINE.coordinate = 'J8';
    await request(app)
      .post('/game/place')
      .set({
        'content-type': 'application/json',
      })
      .send(SUBMARINE)
      .expect('Content-Type', /json/)
      .expect(200);

    const DESTROYER = {
      'boardId': body.boardId,
      'defenderId': body.defenderId,
      'shipType': 'DESTROYER',
      'arrange': 'vertical',
      'coordinate': 'A7',
    };

    DESTROYER.coordinate = 'I1';
    await request(app)
      .post('/game/place')
      .set({
        'content-type': 'application/json',
      })
      .send(DESTROYER)
      .expect('Content-Type', /json/)
      .expect(200);

    DESTROYER.coordinate = 'I3';
    await request(app)
      .post('/game/place')
      .set({
        'content-type': 'application/json',
      })
      .send(DESTROYER)
      .expect('Content-Type', /json/)
      .expect(200);

    DESTROYER.coordinate = 'I5';
    let res = await request(app)
      .post('/game/place')
      .set({
        'content-type': 'application/json',
      })
      .send(DESTROYER)
      .expect('Content-Type', /json/)
      .expect(200);
    expect(res.body.status).toBe('ok');
    expect(res.body.message).toBe('ships placed');
    expect(res.body.finishPlacement).toBe(true);

    let statusRes = await request(app)
      .get(`/game/status?boardId=${body.boardId}`)
      .expect('Content-Type', /json/)
      .expect(200);

    expect(statusRes.body.boardId).toBe(body.boardId);
    expect(statusRes.body.status).toBe('READY TO ATTACK');
    expect(statusRes.body.move).toBe(0);
  });
});
