const request = require('supertest');

const app = require('../app');
describe('Ship placement', function () {
  it('should NOT able to place a ship with invalid board', async function (done) {
    const mockPlacement = {
      'boardId': 8888888,
      'defenderId': 8888888,
      'shipType': 'CRUISER',
      'arrange': 'vertical',
      'coordinate': 'B3',
    };

    let res = await request(app)
      .post('/game/place')
      .set({
        'content-type': 'application/json',
      })
      .send(mockPlacement)
      .expect('Content-Type', /json/)
      .expect(404);

    expect(res.body.status).toBe('no');
    expect(res.body.message).toBe('Not found the game board');
    return await done();// eslint-disable-line
  });

  it('should NOT able to place a ship with invalid defender of the game', async function (done) {
    const { body } = await request(app)
      .get('/game/new')
      .expect('Content-Type', /json/)
      .expect(200);

    const mockPlacement = {
      'boardId': body.boardId,
      'defenderId': 8888888,
      'shipType': 'CRUISER',
      'arrange': 'vertical',
      'coordinate': 'B3',
    };

    let res = await request(app)
      .post('/game/place')
      .set({
        'content-type': 'application/json',
      })
      .send(mockPlacement)
      .expect('Content-Type', /json/)
      .expect(403);

    expect(res.body.status).toBe('no');
    expect(res.body.message).toBe('Only valid defender can place the ships.');
    return await done();// eslint-disable-line
  });

  it('should able to place a ship with the valid placement (vertical)', async function (done) {
    const { body } = await request(app)
      .get('/game/new')
      .expect('Content-Type', /json/)
      .expect(200);

    const mockPlacement = {
      'boardId': body.boardId,
      'defenderId': body.defenderId,
      'shipType': 'CRUISER',
      'arrange': 'vertical',
      'coordinate': 'B3',
    };

    await request(app)
      .post('/game/place')
      .set({
        'content-type': 'application/json',
      })
      .send(mockPlacement)
      .expect('Content-Type', /json/)
      .expect(200);
    return await done();// eslint-disable-line
  });

  it('should NOT able to place a ship with invalid coordinate (000)', async function (done) {
    const { body } = await request(app)
      .get('/game/new')
      .expect('Content-Type', /json/)
      .expect(200);

    const mockPlacement = {
      'boardId': body.boardId,
      'defenderId': body.defenderId,
      'shipType': 'BATTLESHIP',
      'arrange': 'vertical',
      'coordinate': '000',
    };

    let res = await request(app)
      .post('/game/place')
      .set({
        'content-type': 'application/json',
      })
      .send(mockPlacement)
      .expect('Content-Type', /json/)
      .expect(400);

    expect(res.body.status).toBe('no');
    expect(res.body.message).toBe('Invalid coordinate');
    return await done();// eslint-disable-line
  });

  it('should NOT able to place a ship with invalid coordinate (empty)', async function (done) {
    const { body } = await request(app)
      .get('/game/new')
      .expect('Content-Type', /json/)
      .expect(200);

    const mockPlacement = {
      'boardId': body.boardId,
      'defenderId': body.defenderId,
      'shipType': 'BATTLESHIP',
      'arrange': 'vertical',
      'coordinate': '',
    };

    let res = await request(app)
      .post('/game/place')
      .set({
        'content-type': 'application/json',
      })
      .send(mockPlacement)
      .expect('Content-Type', /json/)
      .expect(400);

    expect(res.body.status).toBe('no');
    expect(res.body.message).toBe('Invalid coordinate');
    return await done();// eslint-disable-line
  });

  it('should NOT able to place a ship over board (vertical)', async function (done) {
    const { body } = await request(app)
      .get('/game/new')
      .expect('Content-Type', /json/)
      .expect(200);

    const mockPlacement = {
      'boardId': body.boardId,
      'defenderId': body.defenderId,
      'shipType': 'BATTLESHIP',
      'arrange': 'vertical',
      'coordinate': 'I1',
    };

    await request(app)
      .post('/game/place')
      .set({
        'content-type': 'application/json',
      })
      .send(mockPlacement)
      .expect('Content-Type', /json/)
      .expect(400);
    return await done();// eslint-disable-line
  });

  it('should NOT able to place a ship over board (horizontal)', async function (done) {
    const { body } = await request(app)
      .get('/game/new')
      .expect('Content-Type', /json/)
      .expect(200);

    const mockPlacement = {
      'boardId': body.boardId,
      'defenderId': body.defenderId,
      'shipType': 'BATTLESHIP',
      'arrange': 'horizontal',
      'coordinate': 'A9',
    };

    await request(app)
      .post('/game/place')
      .set({
        'content-type': 'application/json',
      })
      .send(mockPlacement)
      .expect('Content-Type', /json/)
      .expect(400);
    return await done();// eslint-disable-line
  });

  it('should able to place 2 ships with the valid placement use same padding (vertical)', async function (done) {
    const { body } = await request(app)
      .get('/game/new')
      .expect('Content-Type', /json/)
      .expect(200);

    const mockPlacement = {
      'boardId': body.boardId,
      'defenderId': body.defenderId,
      'shipType': 'CRUISER',
      'arrange': 'vertical',
      'coordinate': 'B3',
    };

    await request(app)
      .post('/game/place')
      .set({
        'content-type': 'application/json',
      })
      .send(mockPlacement)
      .expect('Content-Type', /json/)
      .expect(200);

    mockPlacement.coordinate = 'B5';
    await request(app)
      .post('/game/place')
      .set({
        'content-type': 'application/json',
      })
      .send(mockPlacement)
      .expect('Content-Type', /json/)
      .expect(200);
    return await done();// eslint-disable-line
  });

  it('should able to place a ship with the valid placement (horizontal)', async function (done) {
    const { body } = await request(app)
      .get('/game/new')
      .expect('Content-Type', /json/)
      .expect(200);

    const mockPlacement = {
      'boardId': body.boardId,
      'defenderId': body.defenderId,
      'shipType': 'CRUISER',
      'arrange': 'horizontal',
      'coordinate': 'B3',
    };

    await request(app)
      .post('/game/place')
      .set({
        'content-type': 'application/json',
      })
      .send(mockPlacement)
      .expect('Content-Type', /json/)
      .expect(200);
    return await done();// eslint-disable-line
  });

  it('should able to place 2 ships with the valid placement use same padding (horizontal)', async function (done) {
    const { body } = await request(app)
      .get('/game/new')
      .expect('Content-Type', /json/)
      .expect(200);

    const mockPlacement = {
      'boardId': body.boardId,
      'defenderId': body.defenderId,
      'shipType': 'CRUISER',
      'arrange': 'horizontal',
      'coordinate': 'B3',
    };

    await request(app)
      .post('/game/place')
      .set({
        'content-type': 'application/json',
      })
      .send(mockPlacement)
      .expect('Content-Type', /json/)
      .expect(200);

    mockPlacement.coordinate = 'D3';
    await request(app)
      .post('/game/place')
      .set({
        'content-type': 'application/json',
      })
      .send(mockPlacement)
      .expect('Content-Type', /json/)
      .expect(200);
    return await done();// eslint-disable-line
  });

  it('should NOT able to place 2 ship with the same area (vertical)', async function (done) {
    const { body } = await request(app)
      .get('/game/new')
      .expect('Content-Type', /json/)
      .expect(200);

    const mockPlacement = {
      'boardId': body.boardId,
      'defenderId': body.defenderId,
      'shipType': 'CRUISER',
      'arrange': 'vertical',
      'coordinate': 'A1',
    };

    await request(app)
      .post('/game/place')
      .set({
        'content-type': 'application/json',
      })
      .send(mockPlacement)
      .expect('Content-Type', /json/)
      .expect(200);

    mockPlacement.coordinate = 'B1';
    await request(app)
      .post('/game/place')
      .set({
        'content-type': 'application/json',
      })
      .send(mockPlacement)
      .expect('Content-Type', /json/)
      .expect(400);
    return await done();// eslint-disable-line
  });

  it('should NOT able to place 2 ship with the same area (horizontal)', async function (done) {
    const { body } = await request(app)
      .get('/game/new')
      .expect('Content-Type', /json/)
      .expect(200);

    const mockPlacement = {
      'boardId': body.boardId,
      'defenderId': body.defenderId,
      'shipType': 'CRUISER',
      'arrange': 'horizontal',
      'coordinate': 'A1',
    };

    await request(app)
      .post('/game/place')
      .set({
        'content-type': 'application/json',
      })
      .send(mockPlacement)
      .expect('Content-Type', /json/)
      .expect(200);

    mockPlacement.coordinate = 'A2';
    await request(app)
      .post('/game/place')
      .set({
        'content-type': 'application/json',
      })
      .send(mockPlacement)
      .expect('Content-Type', /json/)
      .expect(400);
    return await done();// eslint-disable-line
  });

  it('should NOT able to place 2 ship close to each other (vertical)', async function (done) {
    const { body } = await request(app)
      .get('/game/new')
      .expect('Content-Type', /json/)
      .expect(200);

    const mockPlacement = {
      'boardId': body.boardId,
      'defenderId': body.defenderId,
      'shipType': 'CRUISER',
      'arrange': 'vertical',
      'coordinate': 'A1',
    };

    await request(app)
      .post('/game/place')
      .set({
        'content-type': 'application/json',
      })
      .send(mockPlacement)
      .expect('Content-Type', /json/)
      .expect(200);

    mockPlacement.coordinate = 'A2';
    await request(app)
      .post('/game/place')
      .set({
        'content-type': 'application/json',
      })
      .send(mockPlacement)
      .expect('Content-Type', /json/)
      .expect(400);
    return await done();// eslint-disable-line
  });

  it('should NOT able to place 2 ship close to each other (horizontal)', async function (done) {
    const { body } = await request(app)
      .get('/game/new')
      .expect('Content-Type', /json/)
      .expect(200);

    const mockPlacement = {
      'boardId': body.boardId,
      'defenderId': body.defenderId,
      'shipType': 'CRUISER',
      'arrange': 'vertical',
      'coordinate': 'A1',
    };

    await request(app)
      .post('/game/place')
      .set({
        'content-type': 'application/json',
      })
      .send(mockPlacement)
      .expect('Content-Type', /json/)
      .expect(200);

    mockPlacement.coordinate = 'B1';
    await request(app)
      .post('/game/place')
      .set({
        'content-type': 'application/json',
      })
      .send(mockPlacement)
      .expect('Content-Type', /json/)
      .expect(400);
    return await done();// eslint-disable-line
  });

  it('should NOT able to place ship over ship limit (BATTLESHIP=1)', async function (done) {
    const { body } = await request(app)
      .get('/game/new')
      .expect('Content-Type', /json/)
      .expect(200);

    const mockPlacement = {
      'boardId': body.boardId,
      'defenderId': body.defenderId,
      'shipType': 'BATTLESHIP',
      'arrange': 'vertical',
      'coordinate': 'A1',
    };

    await request(app)
      .post('/game/place')
      .set({
        'content-type': 'application/json',
      })
      .send(mockPlacement)
      .expect('Content-Type', /json/)
      .expect(200);

    mockPlacement.coordinate = 'A9';
    const res = await request(app)
      .post('/game/place')
      .set({
        'content-type': 'application/json',
      })
      .send(mockPlacement)
      .expect('Content-Type', /json/)
      .expect(400);

    expect(res.body.status).toBe('no');
    expect(res.body.message).toBe('Exceed ship placement limit: BATTLESHIP max 1 ships');
    return await done();// eslint-disable-line
  });

  it('should NOT able to place invalid ship type', async function (done) {
    const { body } = await request(app)
      .get('/game/new')
      .expect('Content-Type', /json/)
      .expect(200);

    const mockPlacement = {
      'boardId': body.boardId,
      'defenderId': body.defenderId,
      'shipType': 'STAR_DESTOYER',
      'arrange': 'vertical',
      'coordinate': 'A1',
    };

    let res = await request(app)
      .post('/game/place')
      .set({
        'content-type': 'application/json',
      })
      .send(mockPlacement)
      .expect('Content-Type', /json/)
      .expect(400);

    expect(res.body.status).toBe('no');
    expect(res.body.message).toBe('Invalid ship type. Support only BATTLESHIP, CRUISER, SUBMARINE, DESTROYER');
    return await done();// eslint-disable-line
  });

  it('should able to place all ships in fleet', async function (done) {
    const { body } = await request(app)
      .get('/game/new')
      .expect('Content-Type', /json/)
      .expect(200);

    const BATTLESHIP = {
      'boardId': body.boardId,
      'defenderId': body.defenderId,
      'shipType': 'BATTLESHIP',
      'arrange': 'vertical',
      'coordinate': 'A1',
    };

    await request(app)
      .post('/game/place')
      .set({
        'content-type': 'application/json',
      })
      .send(BATTLESHIP)
      .expect('Content-Type', /json/)
      .expect(200);

    const CRUISER = {
      'boardId': body.boardId,
      'defenderId': body.defenderId,
      'shipType': 'CRUISER',
      'arrange': 'vertical',
      'coordinate': 'A3',
    };

    await request(app)
      .post('/game/place')
      .set({
        'content-type': 'application/json',
      })
      .send(CRUISER)
      .expect('Content-Type', /json/)
      .expect(200);

    CRUISER.coordinate = 'A5';
    await request(app)
      .post('/game/place')
      .set({
        'content-type': 'application/json',
      })
      .send(CRUISER)
      .expect('Content-Type', /json/)
      .expect(200);

    const SUBMARINE = {
      'boardId': body.boardId,
      'defenderId': body.defenderId,
      'shipType': 'SUBMARINE',
      'arrange': 'horizontal',
      'coordinate': 'A8',
    };

    await request(app)
      .post('/game/place')
      .set({
        'content-type': 'application/json',
      })
      .send(SUBMARINE)
      .expect('Content-Type', /json/)
      .expect(200);

    SUBMARINE.coordinate = 'C8';
    await request(app)
      .post('/game/place')
      .set({
        'content-type': 'application/json',
      })
      .send(SUBMARINE)
      .expect('Content-Type', /json/)
      .expect(200);

    SUBMARINE.coordinate = 'E8';
    await request(app)
      .post('/game/place')
      .set({
        'content-type': 'application/json',
      })
      .send(SUBMARINE)
      .expect('Content-Type', /json/)
      .expect(200);

    SUBMARINE.coordinate = 'J8';
    await request(app)
      .post('/game/place')
      .set({
        'content-type': 'application/json',
      })
      .send(SUBMARINE)
      .expect('Content-Type', /json/)
      .expect(200);

    const DESTROYER = {
      'boardId': body.boardId,
      'defenderId': body.defenderId,
      'shipType': 'DESTROYER',
      'arrange': 'vertical',
      'coordinate': 'A7',
    };

    DESTROYER.coordinate = 'I1';
    await request(app)
      .post('/game/place')
      .set({
        'content-type': 'application/json',
      })
      .send(DESTROYER)
      .expect('Content-Type', /json/)
      .expect(200);

    DESTROYER.coordinate = 'I3';
    await request(app)
      .post('/game/place')
      .set({
        'content-type': 'application/json',
      })
      .send(DESTROYER)
      .expect('Content-Type', /json/)
      .expect(200);

    DESTROYER.coordinate = 'I5';
    let res = await request(app)
      .post('/game/place')
      .set({
        'content-type': 'application/json',
      })
      .send(DESTROYER)
      .expect('Content-Type', /json/)
      .expect(200);
    expect(res.body.status).toBe('ok');
    expect(res.body.message).toBe('ships placed');
    expect(res.body.finishPlacement).toBe(true);

    let statusRes = await request(app)
      .get(`/game/status?boardId=${body.boardId}`)
      .expect('Content-Type', /json/)
      .expect(200);

    expect(statusRes.body.boardId).toBe(body.boardId);
    expect(statusRes.body.status).toBe('READY TO ATTACK');
    expect(statusRes.body.move).toBe(0);
    return await done();// eslint-disable-line
  });
});
