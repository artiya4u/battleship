const mysql = require('../mysql');

const testDatabase = 'battleshipTestDB';

beforeAll(async function () {
  await mysql.clearDatabase(testDatabase); // Clear database in case of unexpected test stop.
  await mysql.initializeDatabase(testDatabase);
});

afterAll(async function () {
  // You're my wonderwall
  await mysql.clearDatabase(testDatabase);
  await mysql.end();
});
