const request = require('supertest');

const app = require('../app');

describe('Game', function () {
  it('should able to create a new game', async function (done) {
    const { body } = await request(app)
      .get('/game/new')
      .expect('Content-Type', /json/)
      .expect(200);

    expect(body).toHaveProperty('boardId');
    expect(body).toHaveProperty('attackerId');
    expect(body).toHaveProperty('defenderId');
    return await done();// eslint-disable-line
  });

  it('should able get status of a new game', async function (done) {
    const { body } = await request(app)
      .get('/game/status?boardId=1')
      .expect('Content-Type', /json/)
      .expect(200);

    expect(body.boardId).toBe(1);
    expect(body.status).toBe('NEW');
    expect(body.move).toBe(0);
    return await done();// eslint-disable-line
  });

  it('should able get status of a game with fleet status when provide correct defenderId', async function (done) {
    const { body } = await request(app)
      .get('/game/status?boardId=3&defenderId=3')
      .expect('Content-Type', /json/)
      .expect(200);

    expect(body.boardId).toBe(3);
    expect(body.status).toBe('READY TO ATTACK');
    expect(body.move).toBe(10);
    expect(body).toHaveProperty('fleet');
    return await done();// eslint-disable-line
  });

  it('should able get status of a finished game', async function (done) {
    const { body } = await request(app)
      .get('/game/status?boardId=2')
      .expect('Content-Type', /json/)
      .expect(200);

    expect(body.boardId).toBe(2);
    expect(body.status).toBe('GAME ENDED');
    expect(body.move).toBe(100);
    return await done();// eslint-disable-line
  });

  it('should able get status of a on going game', async function (done) {
    const { body } = await request(app)
      .get('/game/status?boardId=3')
      .expect('Content-Type', /json/)
      .expect(200);

    expect(body.boardId).toBe(3);
    expect(body.status).toBe('READY TO ATTACK');
    expect(body.move).toBe(10);
    return await done();// eslint-disable-line
  });

  it('should able handle not found game', async function (done) {
    const { body } = await request(app)
      .get('/game/status?boardId=8888888')
      .expect('Content-Type', /json/)
      .expect(404);

    expect(body).toHaveProperty('status');
    expect(body.status).toBe('no');
    expect(body.message).toBe('Not found the game board');
    return await done();// eslint-disable-line
  });
});
