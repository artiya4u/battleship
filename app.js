const express = require('express');
const cors = require('cors');
const logger = require('morgan');
const gameRouter = require('./routes/game');

const corsOptions = {
  origin: '*',
  optionsSuccessStatus: 200, // some legacy browsers (IE11, various SmartTVs) choke on 204
};

const app = express();
app.use(cors(corsOptions));
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use('/game/', gameRouter);

module.exports = app;
