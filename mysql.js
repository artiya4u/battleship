const mysql = require('mysql');
const util = require('util');
const fs = require('fs');
const path = require('path');
const readFileAsync = util.promisify(fs.readFile);

const pool = mysql.createPool({
  host: process.env.DB_HOST || '127.0.0.1',
  user: process.env.DB_USER || 'battleship',
  password: process.env.DB_PASSWORD || 'BattlePass',
  connectionLimit: 10,
  port: process.env.DB_PORT || 3306,
  flags: 'MULTI_STATEMENTS',
});

pool.query = util.promisify(pool.query);

pool.initializeDatabase = async function (database) {
  let schemaPath = path.join(__dirname, 'schema.sql');
  let sqlFile = await readFileAsync(schemaPath, { encoding: 'utf8' });
  let sql = sqlFile.replace(/battleship/g, database);
  await pool.query(sql);
};

pool.clearDatabase = async function (database) {
  await pool.query(`drop schema if exists ${database};`);
};

module.exports = pool;
