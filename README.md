# BattleShipREST - A RESTFul API for the battleship game.

### Getting start
- Required NodeJS v12+ https://nodejs.org/en/download/
- Install MySQL Server or using MySQL Server from Docker like this:
```shell script
docker run --name=mysql01 -d -p 3306:3306 -e MYSQL_ROOT_PASSWORD=BattlePass mysql/mysql-server
docker exec -it mysql01 mysql -uroot -pBattlePass
```
- Create a new user
```sql
CREATE USER 'battleship'@'%' IDENTIFIED WITH mysql_native_password BY 'BattlePass';
GRANT ALL PRIVILEGES ON *.* TO 'battleship'@'%';
FLUSH PRIVILEGES;
```
- Import sql schema from file `schema.sql` to your database (Optional):
```shell script
mysql -h 127.0.0.1 -u battleship -pBattlePass < schema.sql
```
- Setting up environment variables for database connection on shell like this:
```shell script
export DB_HOST=127.0.0.1
export DB_USER=battleship
export DB_PASSWORD=BattlePass
export DB_SCHEMA=battleship
export DB_PORT=3306
```
- Install dependencies `npm install`
- Run the unit test `npm run test`
- Start the service `npm start`
- Check the [Postman](https://www.postman.com) collection `BattleshipREST.postman_collection.json` to try the game services or read the document below.

### Battleship REST endpoints

#### 1) New game endpoint
Creates a new game session.

`GET /game/new`

Returns
- `boardId` - A game session Id.
- `attackerId` - A token id for the attacker to attack the defender ship.
- `defenderId` - A token id for the defender to place ships and get fleet information.

#### 2) Status endpoint

Gets a state of a game board and a fleet including individual ship states.

`GET /game/status`

Query Params

- `boardId` - The game session id to get status.
- `defenderId` - The token id for defender to get fleet status (Optional).

Returns:

- `boardId` - The game session id.
- `status` - Text status of the game.
- `move` - Number of attack from attacker.
- `state` - 2 dimensions 10x10 array represent state of the board. Each array represent a row on the map. Value `0` = empty(no attack), `1` = hit, `2` = miss, `3` = sunk
- `fleet` - Only show when set correct defenderId. Set of ships in fleet of the game, `coordinate`, `arrange`, `hp` (hitpoint left of the ship 0 mean sunk).


#### 3) Ship placement endpoint

Places a single ship on board.

`POST /game/place`

Input JSON:
- `boardId` - The game session id.
- `defenderId` - A token id for the defender to place ship fleet.
- `shipType` - Ship type to be placed can be only `BATTLESHIP` `CRUISER` `SUBMARINE` `DESTROYER`.
- `arrange` - Ship arrangement can be only `vertical` and `horizontal`.
- `coordinate` - The coordinate of placing ship. The first character is the row in the maps start from A to J. The number after a character is a column starting from 1 to 10. Possible value is between `A1` `A2` `B1` `B2` ... to `J10`.

Example Input JSON:
```json
{
	"boardId": "1",
	"defenderId": "1",
	"shipType": "DESTROYER",
	"arrange": "vertical",
	"coordinate": "A1"
}
```

Returns:
- `status` - String indicates ship placement status can be `ok` or `no`.
- `message` - String description of the placement and can be an error message. 
- `finishPlacement` - Boolean indicates when finishing place all the ship in the fleet.


#### 4) Attack endpoint

Attacks specific coordinates on board.

`POST /game/attack`

Input JSON:

- `boardId` - The game session id.
- `attackerId` - A token id for the attacker to attack ships.
- `coordinate` - The coordinate of placing ship. The first character is the row in the maps start from A to J. The number after a character is a column starting from 1 to 10. Possible value is between `A1` `A2` `B1` `B2` ... to `J10`.

Example input JSON:
```json
{
	"boardId": "3",
	"attackerId": "3",
	"coordinate": "B2"
}
```

Returns:

- `won` - Boolean indicates whether the attacker win after the attack or not.
- `sinkCount` - Number of ships that sunk.
- `hitCount` - Number of the attack that hit the ship.
- `move` - Number of attack attempts.
- `result` - String description of the attack result. Can be:
`Miss` - when the attack misses, 
`Hit` - when the attack hit a ship,
`You just sank a X` - when the attack hit and sank a ship, 
`Win! You have completed the game in X moves` - when the attack hit and sunk all ship in the fleet.

Returns (error):
- `status` - String indicates ship attack status will be `no` when got an error.
- `message` - String error description.
