create schema if not exists battleship collate utf8mb4_0900_ai_ci;
use battleship;

create table if not exists attacks
(
    id int auto_increment primary key,
    coordinate varchar(4) null,
    attackDate datetime default CURRENT_TIMESTAMP null,
    boardId int null
);

create table if not exists boards
(
    id int auto_increment primary key,
    attackerId int null,
    defenderId int null,
    gameStatus int default 0 null,
    move int default 0 null,
    createDate timestamp default CURRENT_TIMESTAMP null
);

create table if not exists placements
(
    id int auto_increment primary key,
    shipType varchar(20) charset utf8 null,
    coordinate varchar(4) charset utf8 null,
    arrange varchar(20) charset utf8 null,
    boardId int null
);

INSERT IGNORE INTO boards (id, attackerId, defenderId, gameStatus, move, createDate) VALUES (1, 1, 1, 0, 0, '2020-03-11 16:08:18');
INSERT IGNORE INTO boards (id, attackerId, defenderId, gameStatus, move, createDate) VALUES (2, 2, 2, 2, 100, '2020-03-11 16:08:18');
INSERT IGNORE INTO boards (id, attackerId, defenderId, gameStatus, move, createDate) VALUES (3, 3, 3, 1, 10, '2020-03-11 16:08:18');

INSERT IGNORE INTO placements (id, shipType, coordinate, arrange, boardId) VALUES (1, 'BATTLESHIP', 'A1', 'vertical', 2);
INSERT IGNORE INTO placements (id, shipType, coordinate, arrange, boardId) VALUES (2, 'CRUISER', 'A3', 'vertical', 2);
INSERT IGNORE INTO placements (id, shipType, coordinate, arrange, boardId) VALUES (3, 'CRUISER', 'A5', 'vertical', 2);
INSERT IGNORE INTO placements (id, shipType, coordinate, arrange, boardId) VALUES (4, 'SUBMARINE', 'A8', 'horizontal', 2);
INSERT IGNORE INTO placements (id, shipType, coordinate, arrange, boardId) VALUES (5, 'SUBMARINE', 'C8', 'horizontal', 2);
INSERT IGNORE INTO placements (id, shipType, coordinate, arrange, boardId) VALUES (6, 'SUBMARINE', 'E8', 'horizontal', 2);
INSERT IGNORE INTO placements (id, shipType, coordinate, arrange, boardId) VALUES (7, 'SUBMARINE', 'J8', 'horizontal', 2);
INSERT IGNORE INTO placements (id, shipType, coordinate, arrange, boardId) VALUES (8, 'DESTROYER', 'I1', 'vertical', 2);
INSERT IGNORE INTO placements (id, shipType, coordinate, arrange, boardId) VALUES (9, 'DESTROYER', 'I3', 'vertical', 2);
INSERT IGNORE INTO placements (id, shipType, coordinate, arrange, boardId) VALUES (10, 'DESTROYER', 'I5', 'vertical', 2);

INSERT IGNORE INTO placements (id, shipType, coordinate, arrange, boardId) VALUES (11, 'BATTLESHIP', 'A1', 'vertical', 3);
INSERT IGNORE INTO placements (id, shipType, coordinate, arrange, boardId) VALUES (12, 'CRUISER', 'A3', 'vertical', 3);
INSERT IGNORE INTO placements (id, shipType, coordinate, arrange, boardId) VALUES (13, 'CRUISER', 'A5', 'vertical', 3);
INSERT IGNORE INTO placements (id, shipType, coordinate, arrange, boardId) VALUES (14, 'SUBMARINE', 'A8', 'horizontal', 3);
INSERT IGNORE INTO placements (id, shipType, coordinate, arrange, boardId) VALUES (15, 'SUBMARINE', 'C8', 'horizontal', 3);
INSERT IGNORE INTO placements (id, shipType, coordinate, arrange, boardId) VALUES (16, 'SUBMARINE', 'E8', 'horizontal', 3);
INSERT IGNORE INTO placements (id, shipType, coordinate, arrange, boardId) VALUES (17, 'SUBMARINE', 'J8', 'horizontal', 3);
INSERT IGNORE INTO placements (id, shipType, coordinate, arrange, boardId) VALUES (18, 'DESTROYER', 'I1', 'vertical', 3);
INSERT IGNORE INTO placements (id, shipType, coordinate, arrange, boardId) VALUES (19, 'DESTROYER', 'I3', 'vertical', 3);
INSERT IGNORE INTO placements (id, shipType, coordinate, arrange, boardId) VALUES (20, 'DESTROYER', 'I5', 'vertical', 3);

INSERT IGNORE INTO attacks (id, coordinate, attackDate, boardId) VALUES (1, 'A1', '2020-03-12 12:04:31', 2);
INSERT IGNORE INTO attacks (id, coordinate, attackDate, boardId) VALUES (2, 'A2', '2020-03-12 12:04:31', 2);
INSERT IGNORE INTO attacks (id, coordinate, attackDate, boardId) VALUES (3, 'A3', '2020-03-12 12:04:31', 2);
INSERT IGNORE INTO attacks (id, coordinate, attackDate, boardId) VALUES (4, 'A4', '2020-03-12 12:04:31', 2);
INSERT IGNORE INTO attacks (id, coordinate, attackDate, boardId) VALUES (5, 'A5', '2020-03-12 12:04:31', 2);
INSERT IGNORE INTO attacks (id, coordinate, attackDate, boardId) VALUES (6, 'A6', '2020-03-12 12:04:31', 2);
INSERT IGNORE INTO attacks (id, coordinate, attackDate, boardId) VALUES (7, 'A7', '2020-03-12 12:04:31', 2);
INSERT IGNORE INTO attacks (id, coordinate, attackDate, boardId) VALUES (8, 'A8', '2020-03-12 12:04:31', 2);
INSERT IGNORE INTO attacks (id, coordinate, attackDate, boardId) VALUES (9, 'A9', '2020-03-12 12:04:31', 2);
INSERT IGNORE INTO attacks (id, coordinate, attackDate, boardId) VALUES (10, 'A10', '2020-03-12 12:04:31', 2);
INSERT IGNORE INTO attacks (id, coordinate, attackDate, boardId) VALUES (11, 'B1', '2020-03-12 12:04:31', 2);
INSERT IGNORE INTO attacks (id, coordinate, attackDate, boardId) VALUES (12, 'B2', '2020-03-12 12:04:31', 2);
INSERT IGNORE INTO attacks (id, coordinate, attackDate, boardId) VALUES (13, 'B3', '2020-03-12 12:04:31', 2);
INSERT IGNORE INTO attacks (id, coordinate, attackDate, boardId) VALUES (14, 'B4', '2020-03-12 12:04:31', 2);
INSERT IGNORE INTO attacks (id, coordinate, attackDate, boardId) VALUES (15, 'B5', '2020-03-12 12:04:31', 2);
INSERT IGNORE INTO attacks (id, coordinate, attackDate, boardId) VALUES (16, 'B6', '2020-03-12 12:04:31', 2);
INSERT IGNORE INTO attacks (id, coordinate, attackDate, boardId) VALUES (17, 'B7', '2020-03-12 12:04:31', 2);
INSERT IGNORE INTO attacks (id, coordinate, attackDate, boardId) VALUES (18, 'B8', '2020-03-12 12:04:31', 2);
INSERT IGNORE INTO attacks (id, coordinate, attackDate, boardId) VALUES (19, 'B9', '2020-03-12 12:04:31', 2);
INSERT IGNORE INTO attacks (id, coordinate, attackDate, boardId) VALUES (20, 'B10', '2020-03-12 12:04:31', 2);
INSERT IGNORE INTO attacks (id, coordinate, attackDate, boardId) VALUES (21, 'C1', '2020-03-12 12:04:31', 2);
INSERT IGNORE INTO attacks (id, coordinate, attackDate, boardId) VALUES (22, 'C2', '2020-03-12 12:04:31', 2);
INSERT IGNORE INTO attacks (id, coordinate, attackDate, boardId) VALUES (23, 'C3', '2020-03-12 12:04:31', 2);
INSERT IGNORE INTO attacks (id, coordinate, attackDate, boardId) VALUES (24, 'C4', '2020-03-12 12:04:31', 2);
INSERT IGNORE INTO attacks (id, coordinate, attackDate, boardId) VALUES (25, 'C5', '2020-03-12 12:04:31', 2);
INSERT IGNORE INTO attacks (id, coordinate, attackDate, boardId) VALUES (26, 'C6', '2020-03-12 12:04:31', 2);
INSERT IGNORE INTO attacks (id, coordinate, attackDate, boardId) VALUES (27, 'C7', '2020-03-12 12:04:31', 2);
INSERT IGNORE INTO attacks (id, coordinate, attackDate, boardId) VALUES (28, 'C8', '2020-03-12 12:04:31', 2);
INSERT IGNORE INTO attacks (id, coordinate, attackDate, boardId) VALUES (29, 'C9', '2020-03-12 12:04:31', 2);
INSERT IGNORE INTO attacks (id, coordinate, attackDate, boardId) VALUES (30, 'C10', '2020-03-12 12:04:31', 2);
INSERT IGNORE INTO attacks (id, coordinate, attackDate, boardId) VALUES (31, 'D1', '2020-03-12 12:04:31', 2);
INSERT IGNORE INTO attacks (id, coordinate, attackDate, boardId) VALUES (32, 'D2', '2020-03-12 12:04:31', 2);
INSERT IGNORE INTO attacks (id, coordinate, attackDate, boardId) VALUES (33, 'D3', '2020-03-12 12:04:31', 2);
INSERT IGNORE INTO attacks (id, coordinate, attackDate, boardId) VALUES (34, 'D4', '2020-03-12 12:04:31', 2);
INSERT IGNORE INTO attacks (id, coordinate, attackDate, boardId) VALUES (35, 'D5', '2020-03-12 12:04:31', 2);
INSERT IGNORE INTO attacks (id, coordinate, attackDate, boardId) VALUES (36, 'D6', '2020-03-12 12:04:31', 2);
INSERT IGNORE INTO attacks (id, coordinate, attackDate, boardId) VALUES (37, 'D7', '2020-03-12 12:04:31', 2);
INSERT IGNORE INTO attacks (id, coordinate, attackDate, boardId) VALUES (38, 'D8', '2020-03-12 12:04:31', 2);
INSERT IGNORE INTO attacks (id, coordinate, attackDate, boardId) VALUES (39, 'D9', '2020-03-12 12:04:31', 2);
INSERT IGNORE INTO attacks (id, coordinate, attackDate, boardId) VALUES (40, 'D10', '2020-03-12 12:04:31', 2);
INSERT IGNORE INTO attacks (id, coordinate, attackDate, boardId) VALUES (41, 'E1', '2020-03-12 12:04:31', 2);
INSERT IGNORE INTO attacks (id, coordinate, attackDate, boardId) VALUES (42, 'E2', '2020-03-12 12:04:31', 2);
INSERT IGNORE INTO attacks (id, coordinate, attackDate, boardId) VALUES (43, 'E3', '2020-03-12 12:04:31', 2);
INSERT IGNORE INTO attacks (id, coordinate, attackDate, boardId) VALUES (44, 'E4', '2020-03-12 12:04:31', 2);
INSERT IGNORE INTO attacks (id, coordinate, attackDate, boardId) VALUES (45, 'E5', '2020-03-12 12:04:31', 2);
INSERT IGNORE INTO attacks (id, coordinate, attackDate, boardId) VALUES (46, 'E6', '2020-03-12 12:04:31', 2);
INSERT IGNORE INTO attacks (id, coordinate, attackDate, boardId) VALUES (47, 'E7', '2020-03-12 12:04:31', 2);
INSERT IGNORE INTO attacks (id, coordinate, attackDate, boardId) VALUES (48, 'E8', '2020-03-12 12:04:31', 2);
INSERT IGNORE INTO attacks (id, coordinate, attackDate, boardId) VALUES (49, 'E9', '2020-03-12 12:04:31', 2);
INSERT IGNORE INTO attacks (id, coordinate, attackDate, boardId) VALUES (50, 'E10', '2020-03-12 12:04:31', 2);
INSERT IGNORE INTO attacks (id, coordinate, attackDate, boardId) VALUES (51, 'F1', '2020-03-12 12:04:31', 2);
INSERT IGNORE INTO attacks (id, coordinate, attackDate, boardId) VALUES (52, 'F2', '2020-03-12 12:04:31', 2);
INSERT IGNORE INTO attacks (id, coordinate, attackDate, boardId) VALUES (53, 'F3', '2020-03-12 12:04:31', 2);
INSERT IGNORE INTO attacks (id, coordinate, attackDate, boardId) VALUES (54, 'F4', '2020-03-12 12:04:31', 2);
INSERT IGNORE INTO attacks (id, coordinate, attackDate, boardId) VALUES (55, 'F5', '2020-03-12 12:04:31', 2);
INSERT IGNORE INTO attacks (id, coordinate, attackDate, boardId) VALUES (56, 'F6', '2020-03-12 12:04:31', 2);
INSERT IGNORE INTO attacks (id, coordinate, attackDate, boardId) VALUES (57, 'F7', '2020-03-12 12:04:32', 2);
INSERT IGNORE INTO attacks (id, coordinate, attackDate, boardId) VALUES (58, 'F8', '2020-03-12 12:04:32', 2);
INSERT IGNORE INTO attacks (id, coordinate, attackDate, boardId) VALUES (59, 'F9', '2020-03-12 12:04:32', 2);
INSERT IGNORE INTO attacks (id, coordinate, attackDate, boardId) VALUES (60, 'F10', '2020-03-12 12:04:32', 2);
INSERT IGNORE INTO attacks (id, coordinate, attackDate, boardId) VALUES (61, 'G1', '2020-03-12 12:04:32', 2);
INSERT IGNORE INTO attacks (id, coordinate, attackDate, boardId) VALUES (62, 'G2', '2020-03-12 12:04:32', 2);
INSERT IGNORE INTO attacks (id, coordinate, attackDate, boardId) VALUES (63, 'G3', '2020-03-12 12:04:32', 2);
INSERT IGNORE INTO attacks (id, coordinate, attackDate, boardId) VALUES (64, 'G4', '2020-03-12 12:04:32', 2);
INSERT IGNORE INTO attacks (id, coordinate, attackDate, boardId) VALUES (65, 'G5', '2020-03-12 12:04:32', 2);
INSERT IGNORE INTO attacks (id, coordinate, attackDate, boardId) VALUES (66, 'G6', '2020-03-12 12:04:32', 2);
INSERT IGNORE INTO attacks (id, coordinate, attackDate, boardId) VALUES (67, 'G7', '2020-03-12 12:04:32', 2);
INSERT IGNORE INTO attacks (id, coordinate, attackDate, boardId) VALUES (68, 'G8', '2020-03-12 12:04:32', 2);
INSERT IGNORE INTO attacks (id, coordinate, attackDate, boardId) VALUES (69, 'G9', '2020-03-12 12:04:32', 2);
INSERT IGNORE INTO attacks (id, coordinate, attackDate, boardId) VALUES (70, 'G10', '2020-03-12 12:04:32', 2);
INSERT IGNORE INTO attacks (id, coordinate, attackDate, boardId) VALUES (71, 'H1', '2020-03-12 12:04:32', 2);
INSERT IGNORE INTO attacks (id, coordinate, attackDate, boardId) VALUES (72, 'H2', '2020-03-12 12:04:32', 2);
INSERT IGNORE INTO attacks (id, coordinate, attackDate, boardId) VALUES (73, 'H3', '2020-03-12 12:04:32', 2);
INSERT IGNORE INTO attacks (id, coordinate, attackDate, boardId) VALUES (74, 'H4', '2020-03-12 12:04:32', 2);
INSERT IGNORE INTO attacks (id, coordinate, attackDate, boardId) VALUES (75, 'H5', '2020-03-12 12:04:32', 2);
INSERT IGNORE INTO attacks (id, coordinate, attackDate, boardId) VALUES (76, 'H6', '2020-03-12 12:04:32', 2);
INSERT IGNORE INTO attacks (id, coordinate, attackDate, boardId) VALUES (77, 'H7', '2020-03-12 12:04:32', 2);
INSERT IGNORE INTO attacks (id, coordinate, attackDate, boardId) VALUES (78, 'H8', '2020-03-12 12:04:32', 2);
INSERT IGNORE INTO attacks (id, coordinate, attackDate, boardId) VALUES (79, 'H9', '2020-03-12 12:04:32', 2);
INSERT IGNORE INTO attacks (id, coordinate, attackDate, boardId) VALUES (80, 'H10', '2020-03-12 12:04:32', 2);
INSERT IGNORE INTO attacks (id, coordinate, attackDate, boardId) VALUES (81, 'I1', '2020-03-12 12:04:32', 2);
INSERT IGNORE INTO attacks (id, coordinate, attackDate, boardId) VALUES (82, 'I2', '2020-03-12 12:04:32', 2);
INSERT IGNORE INTO attacks (id, coordinate, attackDate, boardId) VALUES (83, 'I3', '2020-03-12 12:04:32', 2);
INSERT IGNORE INTO attacks (id, coordinate, attackDate, boardId) VALUES (84, 'I4', '2020-03-12 12:04:32', 2);
INSERT IGNORE INTO attacks (id, coordinate, attackDate, boardId) VALUES (85, 'I5', '2020-03-12 12:04:32', 2);
INSERT IGNORE INTO attacks (id, coordinate, attackDate, boardId) VALUES (86, 'I6', '2020-03-12 12:04:32', 2);
INSERT IGNORE INTO attacks (id, coordinate, attackDate, boardId) VALUES (87, 'I7', '2020-03-12 12:04:32', 2);
INSERT IGNORE INTO attacks (id, coordinate, attackDate, boardId) VALUES (88, 'I8', '2020-03-12 12:04:32', 2);
INSERT IGNORE INTO attacks (id, coordinate, attackDate, boardId) VALUES (89, 'I9', '2020-03-12 12:04:32', 2);
INSERT IGNORE INTO attacks (id, coordinate, attackDate, boardId) VALUES (90, 'I10', '2020-03-12 12:04:32', 2);
INSERT IGNORE INTO attacks (id, coordinate, attackDate, boardId) VALUES (91, 'J1', '2020-03-12 12:04:32', 2);
INSERT IGNORE INTO attacks (id, coordinate, attackDate, boardId) VALUES (92, 'J2', '2020-03-12 12:04:32', 2);
INSERT IGNORE INTO attacks (id, coordinate, attackDate, boardId) VALUES (93, 'J3', '2020-03-12 12:04:32', 2);
INSERT IGNORE INTO attacks (id, coordinate, attackDate, boardId) VALUES (94, 'J4', '2020-03-12 12:04:32', 2);
INSERT IGNORE INTO attacks (id, coordinate, attackDate, boardId) VALUES (95, 'J5', '2020-03-12 12:04:32', 2);
INSERT IGNORE INTO attacks (id, coordinate, attackDate, boardId) VALUES (96, 'J6', '2020-03-12 12:04:32', 2);
INSERT IGNORE INTO attacks (id, coordinate, attackDate, boardId) VALUES (97, 'J7', '2020-03-12 12:04:32', 2);
INSERT IGNORE INTO attacks (id, coordinate, attackDate, boardId) VALUES (98, 'J8', '2020-03-12 12:04:32', 2);
INSERT IGNORE INTO attacks (id, coordinate, attackDate, boardId) VALUES (99, 'J9', '2020-03-12 12:04:32', 2);
INSERT IGNORE INTO attacks (id, coordinate, attackDate, boardId) VALUES (100, 'J10', '2020-03-12 12:04:32', 2);

INSERT IGNORE INTO attacks (id, coordinate, attackDate, boardId) VALUES (101, 'A1', '2020-03-12 12:04:31', 3);
INSERT IGNORE INTO attacks (id, coordinate, attackDate, boardId) VALUES (102, 'A2', '2020-03-12 12:04:31', 3);
INSERT IGNORE INTO attacks (id, coordinate, attackDate, boardId) VALUES (103, 'A3', '2020-03-12 12:04:31', 3);
INSERT IGNORE INTO attacks (id, coordinate, attackDate, boardId) VALUES (104, 'A4', '2020-03-12 12:04:31', 3);
INSERT IGNORE INTO attacks (id, coordinate, attackDate, boardId) VALUES (105, 'A5', '2020-03-12 12:04:31', 3);
INSERT IGNORE INTO attacks (id, coordinate, attackDate, boardId) VALUES (106, 'A6', '2020-03-12 12:04:31', 3);
INSERT IGNORE INTO attacks (id, coordinate, attackDate, boardId) VALUES (107, 'A7', '2020-03-12 12:04:31', 3);
INSERT IGNORE INTO attacks (id, coordinate, attackDate, boardId) VALUES (108, 'A8', '2020-03-12 12:04:31', 3);
INSERT IGNORE INTO attacks (id, coordinate, attackDate, boardId) VALUES (109, 'A9', '2020-03-12 12:04:31', 3);
INSERT IGNORE INTO attacks (id, coordinate, attackDate, boardId) VALUES (110, 'A10', '2020-03-12 12:04:31', 3);
