const mysql = require('../mysql');
const attacks = {};

attacks.getAttacks = async function (gameId) {
  return mysql.query(`
      SELECT *
      FROM attacks
      WHERE boardId = ?
  `, [gameId]);
};

attacks.insertAttack = async function (attack) {
  return mysql.query(`
      INSERT INTO attacks (coordinate, boardId)
      VALUES (?, ?)
  `, [attack.coordinate, attack.boardId]);
};

module.exports = attacks;
