// The game is played with a fleet of the following ships: 1x Battleship, 2x Cruisers, 3x Destroyers and 4x Submarines.
const ships = {
  EMPTY: { id: 0, placeLimit: 100 },
  BATTLESHIP: { id: 1, length: 4, placeLimit: 1 },
  CRUISER: { id: 2, length: 3, placeLimit: 2 },
  SUBMARINE: { id: 3, length: 3, placeLimit: 4 },
  DESTROYER: { id: 4, length: 2, placeLimit: 3 },
  PADDING: { id: -1, length: 1, placeLimit: 100 },
};

ships.fleetSize = function () {
  let count = 0;
  Object.keys(ships).forEach(function (key) {
    if (ships[key].id > 0) {
      count += ships[key].placeLimit;
    }
  });
  return count;
};

module.exports = ships;
