const mysql = require('../mysql');
const ships = require('./Ships');
const Board = {};
const SIZE = 10;

Board.gameState = Object.freeze({
  NEW: 0,
  READY: 1,
  FINISH: 2,
});

Board.gridState = Object.freeze({
  EMPTY: 0,
  HIT: 1,
  MISS: 2,
  SUNK: 3,
});

function initMap (size) {
  const array = new Array(size).fill(0);
  array.forEach((val, index) => {
    array[index] = new Array(size).fill(0);
  });
  return array;
}

function isValidSpace (map, x, y, length, horizontal) {
  if (horizontal) {
    if (x + length > SIZE) {
      return false;
    }
    for (let i = 0; i < length; i += 1) {
      if (map[x + i][y] !== ships.EMPTY.id) {
        return false;
      }
    }
    return true;
  }

  if (y + length > SIZE) {
    return false;
  }
  for (let i = 0; i < length; i += 1) {
    if (map[x][y + i] !== ships.EMPTY.id) {
      return false;
    }
  }
  return true;
}

function getPositionByCoordinate (coordinate) {
  const charCodeA = 65;
  let y = coordinate.charCodeAt(0) - charCodeA;
  let x = parseInt(coordinate.substring(1)) - 1;
  if (x > SIZE || y > SIZE || x < 0 || y < 0 || isNaN(x) || isNaN(y)) {
    throw new Error('Invalid coordinate');
  }
  return { x, y };
}

Board.getEmptyBoard = function () {
  return {
    map: initMap(SIZE),
    state: initMap(SIZE),
  };
};

Board.place = function (board, placements) {
  let shipCount = 0;
  let shipAdded = {};
  board.fleet = {};
  for (let placement of placements) {
    if (shipAdded[placement.shipType] === undefined) {
      shipAdded[placement.shipType] = 0;
    }
    shipAdded[placement.shipType] += 1;
    shipCount += 1;
    let placingShip = ships[placement.shipType];
    if (placingShip === undefined) {
      throw new Error('Invalid ship type. Support only BATTLESHIP, CRUISER, SUBMARINE, DESTROYER');
    }
    if (shipAdded[placement.shipType] > placingShip.placeLimit) {
      throw new Error(`Exceed ship placement limit: ${placement.shipType} max ${placingShip.placeLimit} ships`);
    }
    let shipUid = `${placingShip.id}${shipAdded[placement.shipType]}`;
    board.fleet[shipUid] = { ...placement, hp: placingShip.length };
    let isHorizontal = (placement.arrange === 'horizontal');
    let { x, y } = getPositionByCoordinate(placement.coordinate);

    // check if the space is valid
    if (!isValidSpace(board.map, x, y, placingShip.length, isHorizontal)) {
      throw new Error(`Cannot place ship at ${placement.coordinate}`);
    }
    // if valid, place the ship
    if (board.map[x][y] === ships.EMPTY.id) {
      // place it horizontally
      if (isHorizontal) {
        for (let i = 0; i < placingShip.length; i += 1) {
          if (board.map[x + i] !== undefined) {
            // Place ship
            board.map[x + i][y] = shipUid;
            // Padding sides
            if (board.map[x + i][y - 1] !== undefined) {
              board.map[x + i][y - 1] = ships.PADDING.id;
            }
            if (board.map[x + i][y + 1] !== undefined) {
              board.map[x + i][y + 1] = ships.PADDING.id;
            }
          }
        }

        // Padding start
        if (board.map[x - 1] !== undefined) {
          if (board.map[x - 1][y] !== undefined) {
            board.map[x - 1][y] = ships.PADDING.id;
          }
          if (board.map[x - 1][y - 1] !== undefined) {
            board.map[x - 1][y - 1] = ships.PADDING.id;
          }
          if (board.map[x - 1][y + 1] !== undefined) {
            board.map[x - 1][y + 1] = ships.PADDING.id;
          }
        }

        // Padding end
        if (board.map[x + placingShip.length] !== undefined) {
          if (board.map[x + placingShip.length][y] !== undefined) {
            board.map[x + placingShip.length][y] = ships.PADDING.id;
          }
          if (board.map[x + placingShip.length][y - 1] !== undefined) {
            board.map[x + placingShip.length][y - 1] = ships.PADDING.id;
          }
          if (board.map[x + placingShip.length][y + 1] !== undefined) {
            board.map[x + placingShip.length][y + 1] = ships.PADDING.id;
          }
        }
      } else {
        for (let i = 0; i < placingShip.length; i += 1) {
          // Place ship
          board.map[x][y + i] = shipUid;

          // Padding sides
          if (board.map[x - 1] !== undefined && board.map[x - 1][y + i] !== undefined) {
            board.map[x - 1][y + i] = ships.PADDING.id;
          }
          if (board.map[x + 1] !== undefined && board.map[x + 1][y + i] !== undefined) {
            board.map[x + 1][y + i] = ships.PADDING.id;
          }
        }
        // Padding start
        if (board.map[x][y - 1] !== undefined) {
          board.map[x][y - 1] = ships.PADDING.id;
        }
        if (board.map[x - 1] !== undefined && board.map[x - 1][y - 1] !== undefined) {
          board.map[x - 1][y - 1] = ships.PADDING.id;
        }
        if (board.map[x + 1] !== undefined && board.map[x + 1][y - 1] !== undefined) {
          board.map[x + 1][y - 1] = ships.PADDING.id;
        }

        // Padding end
        if (board.map[x][y + placingShip.length] !== undefined) {
          board.map[x][y + placingShip.length] = ships.PADDING.id;
        }
        if (board.map[x - 1] !== undefined && board.map[x - 1][y + placingShip.length] !== undefined) {
          board.map[x - 1][y + placingShip.length] = ships.PADDING.id;
        }
        if (board.map[x + 1] !== undefined && board.map[x + 1][y + placingShip.length] !== undefined) {
          board.map[x + 1][y + placingShip.length] = ships.PADDING.id;
        }
      }
    }
  }
  return shipCount === ships.fleetSize();
};

function applySink (board, index) {
  for (let i = 0; i < SIZE; i += 1) {
    for (let j = 0; j < SIZE; j += 1) {
      if (board.map[i][j] === index) {
        board.state[i][j] = Board.gridState.SUNK;
      }
    }
  }
}

Board.attack = function (board, attacks) {
  let hitCount = 0;
  let move = 0;
  let sinkCount = 0;
  let won = false;
  let attackResult = '';
  for (let atk of attacks) {
    let { x, y } = getPositionByCoordinate(atk.coordinate);
    if (board.state[x][y] !== Board.gridState.EMPTY) {
      throw new Error(`Cannot attack at the same place that already attack: ${atk.coordinate}`);
    }
    const target = board.map[x][y];
    move += 1;
    if (target > ships.EMPTY.id) { // Hit
      hitCount += 1;
      board.fleet[target].hp -= 1; // Decrease HP
      if (board.fleet[target].hp === 0) { // Ship sank
        applySink(board, target);
        sinkCount += 1;
        if (sinkCount === ships.fleetSize()) { // Win
          won = true;
          attackResult = `Win! You have completed the game in ${move} moves`;
        } else {
          attackResult = `You just sank a ${board.fleet[target].shipType}`;
        }
      } else {
        board.state[x][y] = Board.gridState.HIT;
        attackResult = 'Hit';
      }
    } else { // Padding or empty count as miss
      board.state[x][y] = Board.gridState.MISS;
      attackResult = 'Miss';
    }
  }
  return { won, sinkCount, hitCount, move, result: attackResult };
};

Board.newGame = async function newGame () {
  // Create random player id for attacker and defender.
  let attackerId = Math.floor(Math.random() * 1000000); // Will act as a token for attacker to attack.
  let defenderId = Math.floor(Math.random() * 1000000);
  let result = await this.insertBoard(attackerId, defenderId);
  return { boardId: result.insertId, attackerId, defenderId };
};

Board.gameStatusText = function (number) {
  switch (number) {
  case Board.gameState.NEW:
    return 'NEW';
  case Board.gameState.READY:
    return 'READY TO ATTACK';
  case Board.gameState.FINISH:
    return 'GAME ENDED';
  }
};

Board.getBoardById = async function (id) {
  return (await mysql.query(`
      SELECT *
      FROM boards
      WHERE id = ?
  `, [id]))[0];
};

Board.insertBoard = async function (attackerId, defenderId) {
  return mysql.query(`
      INSERT INTO boards (attackerId, defenderId)
      VALUES (?, ?);
  `, [attackerId, defenderId]);
};

Board.updateBoard = async function (board) {
  return mysql.query(`
      UPDATE boards
      SET gameStatus = ?,
          move       = ?
      WHERE id = ?
  `, [board.gameStatus, board.move, board.id]);
};

module.exports = Board;
