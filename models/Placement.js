const mysql = require('../mysql');
const placement = {};

placement.getPlacements = async function (gameId) {
  return mysql.query(`
      SELECT *
      FROM placements
      WHERE boardId = ?
  `, [gameId]);
};

placement.insertPlacement = async function (placement) {
  return mysql.query(`
      INSERT INTO placements (shipType,
                                         coordinate,
                                         arrange,
                                         boardId)
      VALUES (?, ?, ?, ?)
  `, [placement.shipType, placement.coordinate, placement.arrange, placement.boardId]);
};

module.exports = placement;
